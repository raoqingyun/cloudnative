package com.raoqy.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by x250 on 2018/1/19.
 */
@RestController
@RefreshScope
public class HelloController {

    @Value("${mapName}")
    private String mapName ;

    @RequestMapping("/hello")
    public String greet(){
        return this.mapName + "hello world ";
    }
}
