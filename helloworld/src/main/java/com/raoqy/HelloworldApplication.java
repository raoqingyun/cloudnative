package com.raoqy;

import com.raoqy.filters.ApplicationFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import javax.servlet.DispatcherType;

@SpringBootApplication
@EnableDiscoveryClient
public class HelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldApplication.class, args);
	}


	@Bean
	FilterRegistrationBean appFilterRegistration(){
		ApplicationFilter filter = new ApplicationFilter();
		FilterRegistrationBean filterBean = new FilterRegistrationBean();
		filterBean.setFilter(filter);
		filterBean.addUrlPatterns("/*");
		filterBean.setEnabled(true);
		filterBean.setDispatcherTypes(DispatcherType.REQUEST);
		return filterBean;
	}
}
