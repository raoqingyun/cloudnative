package com.raoqy.filters;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.web.servlet.ServletComponentScan;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;

/**
 * Created by x250 on 2018/1/21.
 */

@WebFilter
public class ApplicationFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("init application filter");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        System.out.println("do filter");
        chain.doFilter(request,response);
    }

    @Override
    public void destroy() {
        System.out.println("destroy");
    }
}
